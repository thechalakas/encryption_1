﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Encryption_1
{
    class Program
    {
        static void Main(string[] args)
        {
            //lets do the symmetric encryption first

            Console.WriteLine("This is the symmetric encryption stuff");
            
            //lets get the user to enter a sentence
            Console.WriteLine("Enter a message");
            string original_string = Console.ReadLine();

            //let me get a symmetric algorithm object
            //later, when we are doing the actual encryption, we will need two things
            //1. an initialization vector
            //2. (secret) Key. 
            //from what I understand, both of them are assigned by default to a random value
            SymmetricAlgorithm temp_algorithm_object = new AesManaged();

            //now getting the encrypted bytes
            //note that the encrypted output only comes in bytes even if the input was string
            byte[] encrypted_byte_of_original_string = lets_do_encryption(temp_algorithm_object, original_string);

            //now lets do the decryption on the encrypted byte
            string decrypted_original_string = lets_do_decryption(temp_algorithm_object, encrypted_byte_of_original_string);

            //lets display the wares

            Console.WriteLine("original string is {0}", original_string);
            Console.WriteLine("encrypted byte array is {0} (oh oh! looks I cannot show the byte array without writing some extra code. we will leave it like this for now because the bell is ringing", encrypted_byte_of_original_string.ToArray());
            Console.WriteLine("string obtained after decryption {0}", decrypted_original_string);

            //now, lets to that assymetric encryption
            Console.WriteLine("This is the asymmetric encryption stuff");

            //now, we need a public key and a private key.
            //Ideally, these should be used in two separate places
            //for the sake of simplicity both of them are here

            //creating a new public private key
            RSACryptoServiceProvider temp_asymmetric_key = new RSACryptoServiceProvider();
            //obtaining the keys from the key provider object
            string temp_public_key = temp_asymmetric_key.ToXmlString(false);//false parameter means, give the public key
            string temp_private_key = temp_asymmetric_key.ToXmlString(true);//true parameter means, give private key




            //now that I have the public and private key, I can perform encryption

            //I will proceed to encrypt with the public key

            //in both encryption and decryption, I am working exclusively with byte streams
            //which means, I need a byte converter that will convert string to byte array
            UnicodeEncoding string_to_byte_converter = new UnicodeEncoding();
            //I also need a byte array to store this converted string
            //so I will create a byte array for the input string
            byte[] input_byte_stream = string_to_byte_converter.GetBytes(original_string);

            //now a byte array that will hold the encrypted data
            byte[] encrypted_byte_stream;

            using (RSACryptoServiceProvider rsa_encrypt_object = new RSACryptoServiceProvider())
            {
                //get the key first
                //since I am encrypting, I will use the public key
                rsa_encrypt_object.FromXmlString(temp_public_key);
                //do the encryption
                //the first paramter contains the byte stream that needs to be encrypted
                //the second is padding that indicates the type of encryption that needs to be done
                encrypted_byte_stream = rsa_encrypt_object.Encrypt(input_byte_stream, false);
            }


            //I will then decrypt it using the private key

            //a byte array to hold the decrypted data
            byte[] decrypted_byte_stream;

            using (RSACryptoServiceProvider rsa_decrypt_object = new RSACryptoServiceProvider())
            {
                //get the key first
                //we are doing decryption here so using private key
                rsa_decrypt_object.FromXmlString(temp_private_key);

                //time to decrypt
                decrypted_byte_stream = rsa_decrypt_object.Decrypt(encrypted_byte_stream, false);
                
            }

            //let me collect the string from that decrypted byte stream
            string decrypted_input_string = string_to_byte_converter.GetString(decrypted_byte_stream);

            Console.WriteLine("The decrypted string is {0}", decrypted_input_string);

                //lets prevent the console from pulling a batman on us
                Console.ReadLine();
        }

        //method for decryption
        private static string lets_do_decryption(SymmetricAlgorithm temp_algorithm_object, byte[] encrypted_byte_of_original_string)
        {
            //this will be the exact opposite of the below encryption method

            //creating the encrypting object
            //since this is symmetric encryption, we will use the same key and IV
            //actually, we have to
            ICryptoTransform decrypting_object = temp_algorithm_object.CreateDecryptor(temp_algorithm_object.Key, temp_algorithm_object.IV);

            //memory stream, just like before
            //this time though, we already have a byte stream that needs to be decrypted.
            //we will feed that to this memory stream
            using (MemoryStream temp_memory_stream_decryption = new MemoryStream(encrypted_byte_of_original_string))
            {
                //as before I need a crypto stream which will do the actual decryption
                //the three parameters are explained in the encryption method below
                //the decrypted data willl now reside in temp_crypto_stream
                using (CryptoStream temp_crypto_stream = new CryptoStream(temp_memory_stream_decryption, decrypting_object, CryptoStreamMode.Read))
                {
                    //now a stream reader to do the reading
                    //and return the decrypted string
                    using (StreamReader read_object = new StreamReader(temp_crypto_stream))
                    {
                        return read_object.ReadToEnd();
                    }
                }
            }

        }

        //method for encryption
        private static byte[] lets_do_encryption(SymmetricAlgorithm temp_algorithm_object, string original_string)
        {
            //creating the encrypting object
            ICryptoTransform encrypting_object = temp_algorithm_object.CreateEncryptor(temp_algorithm_object.Key, temp_algorithm_object.IV);

            //we are dealing with a byte array here, so we will be using a memory stream
            using (MemoryStream temp_memory_stream_encryption = new MemoryStream())
            {
                //now I need to get a cryptology stream which does the actual encryption
                //the first paramter is the memory stream to use
                //the second is the encrypting object which contains the key and also the initialization vector
                //the third is write, meaning we are writing into a file, the encrypted stuff

                //at this point the temp_crypto_stream has all it needs to start encrypting.
                //the encryption stream, the key and mode of writing. 
                //all thats left to do is the actuall encryption and writing the output to a file.
                using (CryptoStream temp_crypto_stream = new CryptoStream(temp_memory_stream_encryption, encrypting_object, CryptoStreamMode.Write))
                {

                    //that part of encryption is already done when you temp_crypto_stream object was created an initialized

                    //now all that is left is to write the encrypted data stream
                    //since we are writing a stream, we will use a stream writer
                    using (StreamWriter write_object = new StreamWriter(temp_crypto_stream))
                    {
                        
                        //the stream now has the encrypted byte array which is filled into the memory stream from earlier.

                        write_object.Write(original_string);

                        //I must add that, I am a little unclear exactly at which point encryptiong is happening. check notes for
                        //more details
                    }
                    //lets return the encrypted byte array
                    return temp_memory_stream_encryption.ToArray();
                }
            }
        }
    }
}
